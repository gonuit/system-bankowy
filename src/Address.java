class Address{
    private String country, resort, street, houseID, flatID;
    private int postCode;
    Address(String country, String resort, String street, String houseID, String flatID, int postCode){
        this.country =country;
        this.resort = resort;
        this.street = street;
        this.houseID = houseID;
        this.flatID = flatID;
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public int getPostCode() {
        return postCode;
    }

    public String getFlatID() {
        return flatID;
    }

    public String getHouseID() {
        return houseID;
    }

    public String getResort() {
        return resort;
    }

    public String getStreet() {
        return street;
    }
}
