import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import javax.jws.soap.SOAPBinding;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Random;

class Database {
    String databasePath;
    private ArrayList<User> data = null;

    Database(String databasePath){
        this.databasePath = databasePath;

    }

    boolean isReaded(){
        return this.data == null ? false : true;
    }

    void create(){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(databasePath, "UTF-8");
            writer.println("");

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("Nie udalo sie utworzyc pliku bazy danych");
            e.printStackTrace(); //do zrobienia
            writer.close();
        }
        this.data = null;
        System.out.println("Pomyslnie utworzono nowa baze danych");
        writer.close();
    }
    int getNewClientId(){

        return this.data == null || this.data.size() == 0 ? 1000 : ((User)this.data.get(this.data.size() - 1)).getClientIdNumber()+1;
    }
    void insert(User user){
        data.add(user);
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(databasePath, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("Nie udalo sie zaktualizowac bazy danych");
            e.printStackTrace();
        }
        Gson gson = new Gson();
        String json = gson.toJson(this.data);
        writer.println(json);
        writer.close();
        System.out.println("Baza danych zostala zaktualizowana");
    }
    void save(){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(databasePath, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            System.out.println("Nie udalo sie zaktualizowac bazy danych");
            e.printStackTrace();
        }
        Gson gson = new Gson();
        String json = gson.toJson(this.data);
        writer.println(json);
        writer.close();
        System.out.println("Baza danych zostala zaktualizowana");
    }
    void read() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(databasePath));
        Type listType = new TypeToken<ArrayList<User>>(){}.getType();
        this.data = new Gson().fromJson(reader, listType);
        System.out.println( "Baza danych zostala pomyslnie odczytana");
        if(data == null){
            System.out.println( "Baza danych jest pusta");
            this.data = new ArrayList<User>(32);
        }
        else{
            System.out.println( "Wczytanych rekordow: " +data.size() );
        }
    }
    void printAll(){
        if(data.size()<1){
            System.out.println("Nic do wyswietlenia");
            return;
        }
        for(int i = 0; i < this.data.size(); i++){
            System.out.println();
            System.out.println("--- "+(i+1)+" ---------------");
            this.data.get(i).print();
        }
    }
    void displayRecord(int recordId){
        User user = (User) data.get(recordId);
        user.print();
    }
    void searchUser() throws Aborted {
        Input input = new Input();
        System.out.println("Wybierz kryterium wyszukiwania:");
        System.out.println("0 - Numer klienta");
        System.out.println("1 - Imie");
        System.out.println("2 - Nazwisko");
        System.out.println("3 - Pesel");
        System.out.println("4 - Adres");
        switch (input.getLine("Wprowadz numer kryterium: ")){
            case "0":{
                int clientNumber = input.getInt("Podaj numer klienta: ",0,true);
                int printed = 0;
                for(int i = 0; i < this.data.size();i++){
                    User user = data.get(i);
                    if(user.getClientIdNumber() == clientNumber){
                        printed++;
                        System.out.println("--- " + printed + " -------------------");
                        user.print();
                    }
                }
                if(printed == 0){
                    System.out.println("Brak pasujacych rekordow");
                }
                else{
                    System.out.println("-----------------------");
                    System.out.println("Wyświetlonych wynikow: " + printed);
                }
                break;
            }
            case "1":{
                String firstName = input.getWord("Podaj imie: ",3,true,true, true);
                int printed = 0;
                for(int i = 0; i < this.data.size();i++){
                    User user = data.get(i);
                    if(user.getName().equals(firstName)){
                        printed++;
                        System.out.println("--- " + printed + " -------------------");
                        user.print();
                    }
                }
                if(printed == 0){
                    System.out.println("Brak pasujacych rekordow");
                }
                else{
                    System.out.println("-----------------------");
                    System.out.println("Wyświetlonych wynikow: " + printed);
                }
                break;
            }
            case "2":{
                String lastName = input.getWord("Podaj nazwisko: ",3,false,true, true);
                int printed = 0;
                for(int i = 0; i < this.data.size();i++){
                    User user = data.get(i);
                    if(user.getLastName().equals(lastName)){
                        printed++;
                        System.out.println("--- " + printed + " -------------------");
                        user.print();
                    }
                }
                if(printed == 0){
                    System.out.println("Brak pasujacych rekordow");
                }
                else{
                    System.out.println("-----------------------");
                    System.out.println("Wyświetlonych wynikow: " + printed);
                }
                break;
            }
            case "3":{
                long pesel = input.getInt("Podaj pesel: ",11,true);
                int printed = 0;
                for(int i = 0; i < this.data.size();i++){
                    User user = data.get(i);
                    if(user.getPesel() == pesel){
                        printed++;
                        System.out.println("--- " + printed + " -------------------");
                        user.print();
                    }
                }
                if(printed == 0){
                    System.out.println("Brak pasujacych rekordow");
                }
                else{
                    System.out.println("-----------------------");
                    System.out.println("Wyświetlonych wynikow: " + printed);
                }
                break;
            }
            case "4":{
                Address address = input.getAddress();
                int printed = 0;
                for(int i = 0; i < this.data.size();i++){
                    User user = data.get(i);
                    if(
                            user.getAddress().getCountry().equals(address.getCountry()) &&
                                    user.getAddress().getResort().equals(address.getResort()) &&
                                    (
                                            user.getAddress().getFlatID() == null ||
                                                    user.getAddress().getFlatID().equals(address.getFlatID())
                                    ) &&
                                    user.getAddress().getHouseID().equals(address.getHouseID()) &&
                                    user.getAddress().getStreet().equals(address.getStreet()) &&
                                    user.getAddress().getPostCode() == address.getPostCode()
                            ){
                                printed++;
                                System.out.println("--- " + printed + " -------------------");
                        user.print();
                    }
                }
                if(printed == 0){
                    System.out.println("Brak pasujacych rekordow");
                }
                else{
                    System.out.println("-----------------------");
                    System.out.println("Wyświetlonych wynikow: " + printed);
                }
                break;
            }
            default:{
                System.out.println("Wprowadzono bledne kryterium");
                throw new Aborted();
            }
        }
    }
    User getUserByClientNumber(int clientIdNumber){
        for(int i = 0; i < this.data.size(); i++){
            User user = data.get(i);
            if(user.getClientIdNumber() == clientIdNumber){
                return user;
            }
        }
        return null;
    }
    boolean deleteUserByClientIdNumber(int clientIdNumber){
        int accountNumber = -1;
        User user;
        for(int i = 0; i < this.data.size(); i++){
            user = data.get(i);
            if(user.getClientIdNumber() == clientIdNumber){
                accountNumber = i;
                break;
            }
        }
        if(accountNumber < 0) {
            return false;
        }else {
            data.remove(accountNumber);
            this.save();
            return true;
        }
    }
    int findIndexByClientIdNumber(int clientIdNumber){
        User user;
        for(int i = 0; i < this.data.size(); i++){
            user = data.get(i);
            if(user.getClientIdNumber() == clientIdNumber){
                return i;
            }
        }
        return -1;
    }
    boolean addMoney(int clientIdNumber, long ammount){
        int accountNumber = -1;
        User user;
        for(int i = 0; i < this.data.size(); i++){
            user = data.get(i);
            if(user.getClientIdNumber() == clientIdNumber){
                accountNumber = i;
                break;
            }
        }
        if(accountNumber < 0) {
            return false;
        }else {
            user =  data.get(accountNumber);
            user.setBalance(user.getBalance() + ammount);
            data.set(accountNumber, user);
            this.save();
            return true;
        }
    }
    boolean removeMoney(int clientIdNumber, long ammount){
        int index = findIndexByClientIdNumber(clientIdNumber);
        if(index < 0){
            System.out.println("Nie znaleziono konta o podanym numerze klienta");
            return false;
        }
        User user = this.data.get(index);
        long money = user.getBalance();
        if(money < ammount){
            System.out.println("Brak wystarczajacych srodkow");
            return false;
        }
        money = money - ammount;
        user.setBalance(money);
        this.data.set(index,user);
        this.save();
        return true;
    }
    void insertRandomUsers(int numberOfUsers){
        if(numberOfUsers > 1000){
            System.out.println("Liczba wprowadzanych rekordow nie moze byc wieksza niz 1000");
            return;
        }
        long started = System.currentTimeMillis();
        System.out.println("Generowanie...");
        String[] names ={
                "Kamil",
                "Agata",
                "Patryk",
                "Dawid",
                "Katarzyna",
                "Łukasz",
                "Tomasz",
                "Bartek",
                "Maksymilian",
                "Marta",
                "Kamila",
                "Piotr",
                "Zdzisław",
                "Jerzy",
                "Marian",
                "Mateusz",
                "Witold",
                "Józef",
                "Krzysztof",
                "Wiktoria",
                "Anastazja",
                "Roksana",
                "Barbara",
                "Judyta"
        };
        String[] lastNames ={
                "Nowak",
                "Zając",
                "Król",
                "Wieczorek",
                "Kowalczyk",
                "Pawlak",
                "Walczak",
                "Stępień",
                "Baran",
                "Kaczmarek",
                "Mazur",
                "Sikora",
                "Michalak",
                "Bąk",
                "Duda",
                "Włodarczyk",
                "Wilk",
                "Lis",
                "Kubiak",
                "Klyta",
                "Mazurek",
                "Krupa",
                "Mróz",
                "Błaszczyk"
        };
        String[] resort ={
                "Łódź",
                "Częstochowa",
                "Warszawa",
                "Kraków",
                "Katowice",
                "Gdańsk"
        };
        String[] street ={
                "Szkolna",
                "Prosta",
                "Piękna",
                "Długa",
                "Krótka",
                "Opawska"
        };
        for(int i = 0; i < numberOfUsers; i++){
            Random generator = new Random();
            int year = generator.nextInt(99 - 70 + 1) + 70;
            int month = generator.nextInt(12 - 10 + 1) + 10;
            int day = generator.nextInt(28 - 10 + 1) + 10;
            int number = generator.nextInt(99950 - 10050 + 1) + 10050;
            long pesel =  Long.parseLong((Integer.toString(year) + Integer.toString(month)+Integer.toString(day)+Integer.toString(number)), 10);
            data.add(new User(
                    names[generator.nextInt(names.length)],
                    lastNames[generator.nextInt(lastNames.length)],
                    pesel,
                    getNewClientId(),
                    new Address("Polska",resort[generator.nextInt(resort.length)], street[generator.nextInt(street.length)],Integer.toString(generator.nextInt(450)),Integer.toString(generator.nextInt(30)),generator.nextInt(90000)+10000),
                    ((long) generator.nextInt(1000000))
                    ));
        }
        save();
        long now = System.currentTimeMillis();
        long time = now - started;
        System.out.println("Pomyslnie stworzonych rekordow: " + numberOfUsers + "\nW czasie: " + time+"ms");
    }
}
