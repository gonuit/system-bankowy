import java.util.InputMismatchException;
import java.util.Scanner;

class Input {
    Scanner scanner = new Scanner(System.in);
    void flush(){
        scanner.nextLine();
    }
    boolean decide(String msg){
        while(true) {
            System.out.print(msg + " [T/N]: ");
            String temp = scanner.next();
            if(temp.equals("t") || temp.equals("T")){
                this.flush();
                return true;
            }
            if(temp.equals("n") || temp.equals("N")){
                this.flush();
                return false;
            }
        }
    }
    String getLine(String description){
        System.out.print(description + ": ");
        return scanner.nextLine();
    }
    String getWord(String description, int minWordLength, boolean oneWord, boolean onlyLetters, boolean firstLetterUpperCase) throws Aborted {
        String word = "";
        do{
            System.out.print(description);
            word = scanner.nextLine();
            Boolean isCorrect = true;
            if(onlyLetters){
                if(oneWord) {
                    isCorrect = word.matches("[\\p{L}]+");
                }
                else{
                    isCorrect = word.matches("^[\\p{L}][- \\p{L}]+");
                }
            }
            //System.out.println( isWord );
            if(isCorrect && word.length() >= minWordLength){
                if(firstLetterUpperCase){
                    word = word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    
                    String[] parts = word.split("-");
                    for(int x = 0; x < parts.length; x++){
                        parts[x] = parts[x].substring(0, 1).toUpperCase() + parts[x].substring(1);
                        if(x+1 < parts.length){
                            parts[x] = parts[x] + "-";
                        }
                    }
                    word = "";
                    for(String x :parts) word += x;
                    parts = word.split(" ");
                    for(int x = 0; x < parts.length; x++){
                        parts[x] = parts[x].substring(0, 1).toUpperCase() + parts[x].substring(1);
                        if(x+1 < parts.length){
                            parts[x] = parts[x] + " ";
                        }
                    }
                    word = "";
                    for(String x :parts) word += x;
                }
                return word;
            }
            else{
                System.out.println("Wprowadzony ciag znakow nie spelnia kryteriow");
                if(!decide("Chcesz sprobowac ponownie?")){
                    throw new Aborted();
                };
            }
        }while(true);
    }
    long getLongInt(String description, int length, boolean isGreaterThanZero) throws Aborted {
        //96122908212 //11 cyfr
        long number = 0;
        do{
            try {
                System.out.print(description);
                number = scanner.nextLong();
                int numberLength = String.valueOf(number).length();
                if(isGreaterThanZero && number < 0){
                    throw new InputMismatchException();
                }
                if (numberLength == length) {
                    this.flush();
                    return number;
                }
                else{
                    throw new InputMismatchException();
                }
            }
            catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Wprowadzony ciąg znaków nie spelnia kryteriow");
                if(isGreaterThanZero){
                    System.out.println("[dodatnia liczba o dlugosci "+length+" znakow]");
                }
                else{
                    System.out.println("[liczba o dlugosci "+length+" znakow]");
                }
                if(!decide("Chcesz sprobowac ponownie?")){
                    throw new Aborted();
                };
            }
        }while(true);
    }
    int getInt(String description, int length, boolean isGreaterThanZero) throws Aborted {
        //96122908212 //11 cyfr
        int number = 0;
        do{
            try {
                System.out.print(description);
                number = scanner.nextInt();
                int numberLength = String.valueOf(number).length();
                if(isGreaterThanZero && number < 0){
                    throw new InputMismatchException();
                }
                if (numberLength == length || length == 0) {
                    this.flush();
                    return number;
                }
                else{
                    throw new InputMismatchException();
                }
            }
            catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Wprowadzony ciąg znaków nie spelnia kryteriow");
                if(isGreaterThanZero && length > 0){
                    System.out.println("[dodatnia liczba całkowita o dlugosci "+length+" znakow]");
                }
                else if(isGreaterThanZero && length == 0){
                    System.out.println("[Liczba całkowita dodatnia]");
                }
                else if(length > 0){
                    System.out.println("[liczba całkowita o dlugosci "+length+" znakow]");
                }
                else if(length == 0){
                    System.out.println("[liczba całkowita]");
                }
                if(!decide("Chcesz sprobowac ponownie?")){
                    throw new Aborted();
                }
            }
        }while(true);
    }
    Address getAddress() throws Aborted {
        String country, resort, houseID, flatID, street;
        int postCode;
        do {
            System.out.println("Wprowadzanie adresu");
            country = getWord("Podaj kraj: ", 3, false, true, true);
            resort = getWord("Podaj miejscowosc: ", 2, false, true, true);
            postCode = getInt("Podaj kod pocztowy bez wprowadzania znaku \"-\": ", 5, true);
            do {
                street = getWord("Podaj nazwe ulicy: ", 3, false, false,true);
                if (street.matches("^[1-9\\p{L}][ \\p{L}0-9-]{2,}")) {
                    break;
                } else {
                    System.out.println("Wprowadzono nie poprawna nazwe ulicy");
                    if (!decide("Chcesz sprobowac ponownie?")) {
                        throw new Aborted();
                    }
                }
            } while (true);
            do {
                houseID = getWord("Podaj numer domu: ", 1, true, false, true);
                if (houseID.matches("^[1-9][0-9]*[\\p{L}]?$")) {
                    break;
                } else {
                    System.out.println("Wprowadzono nie poprawny numer domu");
                    if (!decide("Chcesz sprobowac ponownie?")) {
                        throw new Aborted();
                    }
                }
            } while (true);
            flatID = null;
            if (decide("Chcesz wprowadzic numer mieszkania?")) {
                flatID = getWord("Podaj numer/nazwe [identyfikator] mieszkania: ", 1, false, false, false);
            }
            System.out.println("-----------------------------------------");
            System.out.println("Kraj: " + country);
            System.out.println("Miejscowosc: " + resort);
            String postCodeTemp = Integer.toString(postCode);
            System.out.println("Kod pocztowy: " + postCodeTemp.substring(0, 2) + "-" + postCodeTemp.substring(2, 5));
            System.out.println("Nazwa ulicy: " + street);
            System.out.println("Numer domu: " + houseID);
            System.out.println("Mieszkanie: " + (flatID == null ? "Nie dotyczy" : flatID));
            System.out.println("-----------------------------------------");
            if(decide("Potwierdzasz wprowadzone dane?")){
                return new Address(country,resort,street,houseID, flatID, postCode);
            }
            else{
                if(!decide("Chcesz wprowadzić dane jeszcze raz?")){
                    throw new Aborted();
                }
            }
        }while(true);
    }
    long getMoney(String description) throws Aborted {
        do{
            System.out.print(description);
            String in = scanner.nextLine();
            boolean isCorrect = (in.matches("[1-9][0-9]*") || in.matches("[0-9]+[.,][0-9]{2}"));
            if(isCorrect){
                String[] parts = in.split("[.,]");
                if(parts.length > 1){
                    if(parts[1].length() < 2){
                        parts[1] = parts[1]+"0";
                    }
                    String finalString = "";
                    for(String x : parts) finalString += x;
                    return Long.parseLong(finalString);
                }
                else {
                    long money = Long.parseLong(in);
                    money *=100;
                    return money;
                }
            }
            else{
                System.out.println("Wprowadziles nie poprawna kwote");
                System.out.println("Poprawny format to np 1000.23 lub 1242");
                System.out.println("[liczba zlotych]<kropka lub przecinek>[2 cyfry jako grosze]");
                System.out.println("lub");
                System.out.println("[liczba zlotych]");
                if(!decide("Chcesz wprowadzic kwote ponownie?")) throw new Aborted();
            }
        }while (true);
    }
}