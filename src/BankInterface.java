import java.io.FileNotFoundException;

class BankInterface{
    BankInterface(){
        welcomeMsg();
        console();
    }
    private String currentOperation ="";
    private Input input = new Input();
    Database db = new Database("database.json");
    private void welcomeMsg(){
        System.out.println("-----------------------------------------");
        System.out.println("Witaj w systemie bankowym");
        System.out.println("Pomoc dostepna po wybraniu komendy: \"pomoc\" lub \"p\"");
        System.out.println("-----------------------------------------");
    }
    private void cancel(){
        System.out.println("Przerwano operacje: ["+ this.currentOperation +"]");
    }
    private User createUser() throws Aborted {
        String firstName, lastName;
        long pesel;
        do {
            //imie
            firstName = input.getWord("Podaj imie: ", 3, true, true, true);
            //nazwisko
            lastName = input.getWord("Podaj nazwisko: ", 2, false, true, true);
            //pesel
            pesel = input.getLongInt("Podaj pesel: ", 11, true);
            System.out.println("-----------------------------------------");
            System.out.println("Imie: " + firstName);
            System.out.println("Nazwisko: " + lastName);
            System.out.println("Pesel: " + pesel);
            System.out.println("-----------------------------------------");
            if(input.decide("Potwierdzasz wprowadzone dane?")){
                break;
            }
            else{
                if(!input.decide("Chcesz wprowadzić dane jeszcze raz?")){
                    throw new Aborted();
                }
            }
        }while(true);
        Address address = input.getAddress();
        return new User(firstName,lastName, pesel, db.getNewClientId(),address, 0);

    }
    private boolean databaseReady(){
        if (!db.isReaded()) {
            System.out.println("Baza danych nie została odczytana");
            System.out.println("Zanim wykonasz to polecenie wczytaj baze danych, polecenie: \"wbd\"");
            return false;
        }
        return true;
    }
    private void console(){
        do {
            try {

                //input.getWord();
                switch (input.getLine("Wprowadz komende")) {
                    case "wyjdz":
                    case "w":
                    case "W": {
                        if (!input.decide("Potwierdzasz zamkniecie systemu bankowego?")) {
                            cancel();
                            break;
                        }
                        System.out.println("Zamykanie systemu bankowego...");
                        System.out.println("Do zobaczenie wkrotce");
                        System.exit(0);
                    }
                    case "pomoc":
                    case "p":
                    case "P": {
                        System.out.println("-----------------------------------------");
                        System.out.println("POMOC");
                        System.out.println("-----------------------------------------");
                        System.out.println("\"pomoc\" / \"p\" - otwiera pomoc");
                        System.out.println("\"wyjdz\" / \"w\" - konczy dzialanie programu");
                        System.out.println("\"wczytaj baze danych\" / \"wbd\" - Czyta dane z bazy danych");
                        System.out.println("\"stworz baze danych\" / \"sbd\" - Tworzy nowa baze danych / Usuwa wszystkie dane z bazy danych");
                        System.out.println("\"stworz uzytkownika\" / \"su\" - Tworzy nowe konto uzytkownika");
                        System.out.println("\"usun uzytkownika\" / \"uu\" - Usuwa uzytkownika");
                        System.out.println("\"wyswietl informacje\" / \"wi\" - Wyswietla uzytkownikow o podanych kryteriach");
                        System.out.println("\"pokaz wszystkie\" / \"pw\" - Wyswietla wszystkich uzytkownikow");
                        System.out.println("\"wyplac pieniadze\" / \"wyp\" - Wyplac odpowiednia kwote z konta");
                        System.out.println("\"wplac pieniadze\" / \"wpp\" - Wplac odpowiednia kwote na konto");
                        System.out.println("\"przelej pieniadze\" / \"pp\" - Przelej pieniadze miedzy dwoma kontami");
                        System.out.println("\"zapisz\" / \"za\" - Zapisuje aktualny postep prac do bazy danych");
                        System.out.println("\"inicjuj\" / \"ini\" - Zainicjuj baze danych losowymi danymi uzytkowniko");
                        System.out.println("-----------------------------------------");
                        break;
                    }
                    case "inicjuj":
                    case "ini":{
                        this.currentOperation = "Inicjuj baze losowymi kontami";
                        if (!databaseReady()) break;
                        if (!input.decide("Czy na pewno chcesz dodac do bazy danych losowe konta uzytkownikow?")) {
                            cancel();
                            break;
                        }
                        db.insertRandomUsers(input.getInt("Ile uzytkownikow generowac (maksymalnie 1000)?: ",0,true));
                        break;
                    }
                    case "wczytaj baze danych":
                    case "wbd": {
                        this.currentOperation = "Wczytywanie bazy danych";
                        if (!input.decide("Czy na pewno chcesz wczytac baze danych?")) {
                            cancel();
                            break;
                        }
                        db.read();

                        break;
                    }
                    case "zapisz":
                    case "za":{
                        this.currentOperation = "Aktualizacja bazy danych";
                        if (!databaseReady()) break;
                        if(!input.decide("Czy chcesz zaktualizowac baze danych? (zapisac aktualny postep)")){
                            cancel();
                            break;
                        }
                        db.save();
                        break;
                    }
                    case "sbd":
                    case "stworz baze danych": {
                        System.out.println("Rozpoczeto tworzenie bazy danych");
                        if (input.decide("Jesli baza danych juz istnieje ten proces usunie wszystkie dotychczasowe dane, mimo to kontynuowac?")) {
                            System.out.println("Tworzenie...");
                            db.create();
                        }
                        break;
                    }
                    case "usun uzytkownika":
                    case "uu": {
                        this.currentOperation = "Usuwanie konta";
                        if (!databaseReady()) break;
                        if (!input.decide("Czy rozpoczac usuwanie uzytkownika?")) {
                            cancel();
                            break;
                        }
                        int id = input.getInt("Podaj numer klienta, konta ktore chcesz usunac: ", 0, true);
                        User user = db.getUserByClientNumber(id);
                        if (user == null) {
                            System.out.println("Konto o podanym numerze klienta nie istnieje");
                            cancel();
                            break;
                        }
                        user.print();
                        if (input.decide("Czy usunac podane konto?")) {
                            System.out.println("Usuwanie...");
                            if (db.deleteUserByClientIdNumber(id)) {
                                System.out.println("Konto zostalo pomyslnie usuniete");
                                break;
                            } else {
                                System.out.println("Podczas usuwania konta wystapil blad");
                                cancel();
                                break;
                            }
                        } else {
                            cancel();
                            break;
                        }
                    }
                    case "wyswietl informacje":
                    case "wi": {
                        this.currentOperation = "Wyszukiwanie uzytkownikow";
                        if (!databaseReady()) break;

                        if (!input.decide("Czy chcesz rozpoczac wyszukiwanie?")) {
                            cancel();
                            break;
                        }
                        db.searchUser();

                        break;
                    }
                    case "stworz uzytkownika":
                    case "su": {
                        this.currentOperation = "Tworzenie konta";
                        if (!databaseReady()) {
                            break;
                        }
                        if (!input.decide("Czy chcesz rozpoczac tworzenie konta?")) {
                            cancel();
                            break;
                        }
                        else{
                            System.out.println("Rozpoczeto tworzenie konta");
                            User user = createUser();
                            user.print();
                            if (!input.decide("Czy utworzyc podane konto?")) {
                                cancel();
                                break;
                            }
                            db.insert(user);
                            System.out.println("Twoj numer uzytkownika to: " + user.getClientIdNumber());
                            break;
                        }
                    }
                    case "pokaz wszystkie":
                    case "pw": {
                        this.currentOperation = "Wyswietlanie wszystkich rekordow";
                        if (!databaseReady()) {
                            break;
                        }
                        if (input.decide("Chcesz wyswietlic wszystkie rekordy?")) {
                            db.printAll();
                        } else {
                            cancel();
                        }
                        break;
                    }
                    case "przelej pieniadze":
                    case "pp": {
                        this.currentOperation = "Przelej pieniadze na konto";
                        if (!databaseReady()) break;
                        if (!input.decide("Czy rozpoczac transfer pieniedzy miedzy kontami?")) {
                            cancel();
                            break;
                        }
                        int idf = input.getInt("Podaj imie klienta z ktorgo konta bedziesz przelewal pieniadze: ", 0, true);
                        int idt = input.getInt("Podaj numer klienta na ktore chcesz wplacic pieniadze: ", 0, true);
                        if (idf == idt) {
                            System.out.println("Podano dwa takie same konta");
                            cancel();
                            break;
                        }
                        User userF = db.getUserByClientNumber(idf);
                        User userT = db.getUserByClientNumber(idt);
                        if (userF == null || userT == null) {
                            System.out.println("Konto o podanym numerze klienta nie istnieje");
                            cancel();
                            break;
                        }
                        long money = input.getMoney("Podaj kwote: ");

                        if (db.removeMoney(idf, money)) {
                            System.out.println("Pieniadze zostaly pomyslnie wyplacone z konta: " + idf);
                        } else {
                            System.out.println("Podczas wplacania wystapil blad, sprobuj ponownie");
                            cancel();
                            break;
                        }
                        if (db.addMoney(idt, money)) {
                            System.out.println("Pieniadze zostaly pomyslnie wplacone na konto: " + idt);
                        } else {
                            System.out.println("Podczas wplacania wystapil blad, sprobuj ponownie");
                            System.out.println("Zwracanie pieniedzy na konto...");
                            db.addMoney(idf, money);
                            cancel();
                            break;
                        }
                        break;
                    }
                    case "wplac pieniadze":
                    case "wpp": {
                        this.currentOperation = "Wplata pieniedzy na konto";
                        if (!databaseReady()) break;
                        if (!input.decide("Czy chcesz rozpoczac wplacanie pieniedzy?")) {
                            cancel();
                            break;
                        }
                        int id = input.getInt("Podaj numer klienta na ktore chcesz wplacic pieniadze: ", 0, true);
                        User user = db.getUserByClientNumber(id);
                        if (user == null) {
                            System.out.println("Konto o podanym numerze klienta nie istnieje");
                            cancel();
                            break;
                        }


                        long money = input.getMoney("Podaj kwote: ");
                        if (db.addMoney(id, money)) {
                            System.out.println("Pieniadze zostaly pomyslnie wplacone");
                        } else {
                            System.out.println("Podczas wplacania wystapil blad, sprobuj ponownie");
                            cancel();
                            break;
                        }
                        break;
                    }
                    case "wyplac pieniadze":
                    case "wyp": {
                        this.currentOperation = "Wypłata pieniedzy z konta";
                        if (!databaseReady()) break;
                        if (!input.decide("Czy rozpoczac wyplacanie pieniedzy?")) {
                            cancel();
                            break;
                        }
                        int id = input.getInt("Podaj numer klienta z ktorego chcesz wyplacic pieniadze: ", 0, true);
                        User user = db.getUserByClientNumber(id);
                        if (user == null) {
                            System.out.println("Konto o podanym numerze klienta nie istnieje");
                            cancel();
                            break;
                        }
                        long money = input.getMoney("Podaj kwote: ");
                        if (db.removeMoney(id, money)) {
                            System.out.println("Pieniadze zostaly pomyslnie wyplacone");
                        } else {
                            System.out.println("Podczas wyplacania wystapil blad, sprobuj ponownie");
                            cancel();
                            break;
                        }
                        break;
                    }
                    default: {
                        System.out.println("Wpisano bledna komende");
                        System.out.println("Sprobuj wpisac: \"pomoc\" lub \"p\"");
                        break;
                    }
                }
            }
            catch (FileNotFoundException e) {
                System.out.println("Nie odnaleziono bazy danych");
                System.out.println("Aby utworzyc nowa baze danych uzyj komendy \"stworz baze danych\" lub \"sbd\"");
            }
            catch (NumberFormatException e) {
                System.out.println("Podana liczba nie spelnia kryteriow naszego konta bankowego");
                cancel();
            }
            catch (Aborted aborted) {
                cancel();
            }
            catch (OutOfMemoryError e) {
                System.out.println("Brak dostepnej pamieci");
                cancel();
            }
            catch (Exception e){
                System.out.println("OJ! Cos poszlo nie tak");
                System.out.println("Sprobuj ponownie");
            }
        } while (true);
    }
}
