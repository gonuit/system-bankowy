class User{
    private String name, lastName;
    private int clientIdNumber;
    long pesel;
    long balance;
    Address address;
    User(String name, String lastName, long pesel, int clientIdNumber, Address address, long balance){
        this.name = name;
        this.lastName = lastName;
        this.pesel = pesel;
        this.clientIdNumber = clientIdNumber;
        this.balance = balance;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getLastName() {
        return lastName;
    }

    public long getPesel() {
        return pesel;
    }

    public Address getAddress() {
        return address;
    }

    public int getClientIdNumber() {
        return clientIdNumber;
    }

    public long getBalance() {
        return balance;
    }
    public String displayBalance(){
        if(balance == 0) return "0.00";
        String money = String.valueOf(balance);
        return money.substring(0, money.length()-2) + "." + money.substring(money.length()-2);
    }
    public void print(){
        System.out.println("Numer klienta: " + clientIdNumber);
        System.out.println("Saldo: " + displayBalance());
        System.out.println("Imie: " + name);
        System.out.println("Nazwisko: " + lastName);
        System.out.println("Pesel: " + pesel);
        System.out.println("Kraj: " + address.getCountry());
        System.out.println("Miejscowosc: " + address.getResort());
        String postCode = Integer.toString(address.getPostCode());
        System.out.println("Kod pocztowy: " + postCode.substring(0, 2) + "-" + postCode.substring(2, 5));
        System.out.println("ulica: " + address.getStreet());
        System.out.println("Numer domu: " + address.getHouseID());
        System.out.println("Mieszkanie: " + (address.getFlatID() == null ? "Nie dotyczy" : address.getFlatID()));
    }
}
